use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InitRegistrationResponse {
    pub status_code: u32,
    pub reg_token: String,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegistrationRequest<'a> {
    #[serde(rename(serialize = "APIKey"))]
    pub api_key: &'a str,
    pub reg_token: &'a str,
    pub finalize_registration: bool,
    pub email: &'a str,
    pub password: &'a str,
    pub data: String,
    pub profile: String,
    pub http_status_codes: bool,
}

#[derive(Serialize)]
pub struct RegistrationRequestData<'a> {
    pub first_visit_vl: &'a str,
    pub privacy_v_2: bool,
    pub terms_vl: bool,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegistrationRequestProfile<'a> {
    pub first_name: Option<&'a str>,
    pub last_name: Option<&'a str>,
    pub gender: &'a str,
    pub birth_day: u32,
    pub birth_month: u32,
    pub birth_year: u32,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegistrationResponse {
    pub status_code: u32,
    #[serde(rename(deserialize = "UID"))]
    pub uid: String,
}

#[derive(Deserialize)]
pub struct AppConfig {
    pub api_key: String,
    pub email: String,
    pub password: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
}

#[derive(Debug)]
pub struct Registration {
    pub email: String,
    pub uid: String,
}
