mod types;
use chrono::Utc;
use config::{Config, ConfigBuilder, File};
use reqwest::{self, Client, Url};
use serde_json::json;
use std::env;
use std::error::Error;
use types::*;

const GIGYA_BASE_URL: &str = "https://accounts.eu1.gigya.com/";
const CONFIG_FILENAME: &str = "vlaccount_config.yaml";
type MyResult<T> = Result<T, Box<dyn Error>>;

#[tokio::main]
async fn main() -> MyResult<()> {
    let config = load_config();
    let client = Client::new();

    let reg_token = init_registration(&client, &config).await?;
    let registration = register(&client, &config, &reg_token).await?;

    println!("{:?}", registration);
    Ok(())
}

async fn init_registration(client: &Client, config: &AppConfig) -> MyResult<String> {
    let url = Url::parse(&format!("{}accounts.initRegistration", GIGYA_BASE_URL))?;
    let params = [("APIKey", &config.api_key)];

    let response = client.get(url).query(&params).send().await?;
    let response = response.json::<InitRegistrationResponse>().await?;

    match response.status_code {
        200 => Ok(response.reg_token),
        x => Err(Box::from(format!("Init registration status code {}", x))),
    }
}

async fn register(
    client: &Client,
    config: &AppConfig,
    reg_token: &String,
) -> MyResult<Registration> {
    let url = Url::parse(&format!("{}accounts.register", GIGYA_BASE_URL))?;
    let email = generate_email(&config.email);
    let params = RegistrationRequest {
        api_key: &config.api_key,
        reg_token,
        finalize_registration: true,
        email: &email,
        password: &config.password,
        data: json!(RegistrationRequestData {
            first_visit_vl: "2020-11-27T11:21:45.613Z",
            privacy_v_2: true,
            terms_vl: true,
        })
        .to_string(),
        profile: json!(RegistrationRequestProfile {
            first_name: config.first_name.as_ref().map(|x| x.as_str()),
            last_name: config.last_name.as_ref().map(|x| x.as_str()),
            gender: "m",
            birth_day: 1,
            birth_month: 2,
            birth_year: 2000,
        })
        .to_string(),
        http_status_codes: true,
    };

    let response = client.post(url).form(&params).send().await?;
    let response = response.json::<RegistrationResponse>().await?;

    match response.status_code {
        206 => Ok(Registration {
            uid: response.uid,
            email,
        }),
        x => Err(Box::from(format!("Registration status code {}", x))),
    }
}

fn generate_email(email_template: &str) -> String {
    let time = Utc::now().format("%Y%m%dT%H%M%S").to_string();
    email_template.replace("$", &time)
}

fn load_config() -> AppConfig {
    let config_filename = {
        let mut path = env::current_dir().unwrap().join(CONFIG_FILENAME);
        if !path.exists() {
            path = env::current_exe().unwrap();
            path.set_file_name(CONFIG_FILENAME);
        }
        path
    };

    let config = Config::builder()
        .add_source(File::with_name(
            config_filename
                .into_os_string()
                .into_string()
                .unwrap()
                .as_str(),
        ))
        .build()
        .expect("Can't load config");

    let mut app_config: AppConfig = config
        .try_deserialize()
        .expect("Failed to deserialize configuration");

    if let Some(email) = get_email_from_args() {
        app_config.email = email;
    }

    app_config
}

fn get_email_from_args() -> Option<String> {
    let index = env::args().position(|x| x == "-e");
    index.and_then(|i| env::args().nth(i + 1))
}
